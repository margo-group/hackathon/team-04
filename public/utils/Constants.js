const Constants = {

    HTTPCode: {
        SUCCESS: 200,
        BAD_REQUEST: 400
    },

 CERTIFICATE_ARRAY: ["firstname", "middlename", "lastname", "gender", "birthPlace", "birthdate", "parent1", "parent2", "dateRecognitionParent1", "dateRecognitionParent2"],
 RETRIEVE_CERTIFICATE_ARRAY: ["firstname", "middlename", "lastname", "gender", "birthPlace", "birthdate"]

};

module.exports = Constants;
