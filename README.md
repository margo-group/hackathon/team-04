# hackathon_unicef

Ce projet a été mis en place pour répondre aux problématiques posées par l’UNICEF, à savoir :
- 230 millions d’enfants n’ont pas d’Etat Civil
- Certains certificats d’EC manquent de preuve d’authenticité
- Pas de connexion ni de smartphone

Nous avions pour objectif de développer sous 48h une blockchain permettant d’enregistrer l’EC de l’enfant, son carnet de santé, ses acquis de compétences ainsi que ses diplômes.

La solution proposée est l’usage de SMS par l’officier d’état civil, accrédité par l’UNICEF. Présent sur le terrain, il envoie sous forme de SMS les données pour compléter l’EC de l’enfant dans la blockchain.